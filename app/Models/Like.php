<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Like extends Model
{
    use HasFactory,Uuids;


    protected $table = 'likes';
    protected $keyType = 'string';


    public $incrementing = false;


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'like',
        'likeble_id',
        'likeble_type',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function likeable(){
        return $this->morphTo();
    }
}
