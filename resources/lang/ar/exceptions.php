<?php 
return array (
  'record_not_found' => 'لم يتم العثور على السجل',
  'bad_request' => 'طلب خاطئ',
  'login_required' => 'يجب عليك تسجيل الدخول',
  'forbidden' => 'أنت غير مصرح لهذا الإجراء',
);