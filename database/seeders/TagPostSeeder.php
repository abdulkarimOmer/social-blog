<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Post;


class TagPostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$posts = Post::select('id')->get();
        //$tags = Tag::select('id')->get();
        $posts = Post::all();

        foreach($posts as $post){
            $tagIds = Tag::inRandomOrder()->take(rand(1,5))->pluck('id');
            //dd($tagIds);
            $post->tags()->sync($tagIds);
        }
    }
}
