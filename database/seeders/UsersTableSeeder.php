<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        //Admin User Seed
       // factory(User::class)->state('admin')->create();
        User::factory()->times(100)->create();
    }
}
