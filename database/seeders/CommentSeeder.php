<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Comment;
use App\Models\Post;
use App\Models\User;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = Post::select('id')->get();
        $users = User::select('id')->get();
        $comments = Comment::factory()->times(100)->make([
            'user_id' => $users->random()->id,
            'post_id' => $posts->random()->id,
        ])->toArray();

        Comment::insert($comments);

    }
}
