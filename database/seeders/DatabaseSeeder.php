<?php

use App\Models\Category;
use App\Models\Post;
use Database\Seeders\CategorySeeder;
use Database\Seeders\CommentSeeder;
use Database\Seeders\LikeSeeder;
use Database\Seeders\PostSeeder;
use Database\Seeders\TagSeeder;
use Database\Seeders\UsersTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             UsersTableSeeder::class,
             CategorySeeder::class,
             TagSeeder::class,
             PostSeeder::class,
             CommentSeeder::class,
             LikeSeeder::class
        ]);

    }
}
